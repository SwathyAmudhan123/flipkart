package advanceLevel;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.HomePage;
import wdMethods.ProjectMethods;

public class TestCase extends ProjectMethods {
	@BeforeTest
	public void setData() {
		testCaseName = "TC001_FlipKart";
		testDescription = "Mobile Research";
		authors = "Swathy";
		category = "smoke";
		dataSheetName = "TC001_FlipKart";
		testNodes = "Flipkart";
	}

	@Test
	public void testCase() {
		new HomePage().moveElectronics().clickMi().verifyPageTitle().newestFirst().printProductPriceName()
				.clickFirstProduct(1).verifyTitleOfFirstProductClicked().printCountOfRatingAndReview();
	}
}
