package pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Then;
import wdMethods.ProjectMethods;

public class MiMobilePhonePage extends ProjectMethods {

	@FindBy(xpath = "//div[text()='Newest First']")
	WebElement eleClickNewestFirst;
	@FindBy(tagName = "title")
	WebElement elePageTitle;
	@FindBy(className = "_3wU53n")
	List<WebElement> productName;
	@FindBy(xpath = "//div[@class='_6BWGkk']/div/div[1]")
	List<WebElement> productPrice;
	@FindBy(xpath = "(//div[@class='_3wU53n'])[1]")
	WebElement eleFirstProduct;

	public MiMobilePhonePage() {
		PageFactory.initElements(driver, this);
	}

	@Then("Verify Page title should contains Mi Mobile Phones")
	public MiMobilePhonePage verifyPageTitle() {
		String expectedTitle = "Mi";
		String text = elePageTitle.getText();
		System.out.println("************************" + text);
		// verifyPartialText(elePageTitle, expectedTitle);
		return this;
	}

	@Then("Click on Newest First")
	public MiMobilePhonePage newestFirst() {
		click(eleClickNewestFirst);
		return this;
	}

	@Then("Print all the Product Name along with the price")
	public MiMobilePhonePage printProductPriceName() {
		for (int i = 0; i < productName.size(); i++) {
			System.out.println(productName.get(i).getText() + "   " + productPrice.get(i).getText());
		}
		return this;
	}

	@Then("Click on the first product then switch to window {int}")
	public RedMi6ProPage clickFirstProduct(int index) {
		click(eleFirstProduct);
		switchToWindow(index);
		return new RedMi6ProPage();
	}

}
