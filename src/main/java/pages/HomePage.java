package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import wdMethods.ProjectMethods;

public class HomePage extends ProjectMethods {

	public HomePage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//span[text()='Electronics']")
	WebElement eleClickElectronics;
	@FindBy(linkText = "Mi")
	WebElement eleLinkTextMi;

	@And("Mouse over on Electronics")
	public HomePage moveElectronics() {
		Actions builder = new Actions(driver);
		builder.moveToElement(eleClickElectronics).perform();
		return this;
	}

	@Then("Click on Mi Phone Model")
	public MiMobilePhonePage clickMi() {
		click(eleLinkTextMi);
		return new MiMobilePhonePage();

	}

}
