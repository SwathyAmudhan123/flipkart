package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Then;
import wdMethods.ProjectMethods;

public class RedMi6ProPage extends ProjectMethods {

	@FindBy(xpath = "//span[@class='_35KyD6']")
	WebElement eleFirstProductName;
	@FindBy(xpath = "(//div[@class='col-12-12']/span)[1]")
	WebElement eleRatings;
	@FindBy(xpath = "(//div[@class='col-12-12']/span)[2]")
	WebElement eleReviews;

	public RedMi6ProPage() {
		PageFactory.initElements(driver, this);
	}

	@Then("Verify Page Title is matching based on the product clicked")
	public RedMi6ProPage verifyTitleOfFirstProductClicked() {
		String title = driver.getTitle();
		String productName = eleFirstProductName.getText();
		String replacedText = productName.replaceAll("( .*)", "");
		if (title.contains(replacedText)) {
			System.out.println("Both name are matching");
		} else {
			System.out.println("Both name are not matching");
		}
		return this;
	}

	@Then("Print the count of Ratings and Reviews")
	public RedMi6ProPage printCountOfRatingAndReview() {
		System.out.println(eleRatings.getText());
		System.out.println(eleReviews.getText());
		return this;
	}

}
