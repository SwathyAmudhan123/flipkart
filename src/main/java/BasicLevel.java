import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.Window;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class BasicLevel {

	// public static void main(String[] args) throws InterruptedException
	@Test
	public void basicLevel() throws InterruptedException {
		System.setProperty("webdriver.chromedriver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.flipkart.com");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.getKeyboard().sendKeys(Keys.ESCAPE);
		WebElement eleElectronics = driver.findElementByXPath("//span[text()='Electronics']");
		Actions builder = new Actions(driver);
		builder.moveToElement(eleElectronics).perform();
		driver.findElementByLinkText("Mi").click();
		Thread.sleep(3000);
		String pageTitle = driver.getTitle();
		if (pageTitle.contains("Mi")) {
			System.out.println("Verified the Page title");
		} else {
			System.out.println("Page title didn't match");
		}
		driver.findElementByXPath("//div[text()='Newest First']").click();
		Thread.sleep(3000);
		List<WebElement> productName = driver.findElementsByClassName("_3wU53n");
		List<WebElement> productPrice = driver.findElementsByXPath("//div[@class='_6BWGkk']/div/div[1]");
//		for (WebElement listProductName : productName) {
//			System.out.println(listProductName.getText());
//
//		}
//		for (WebElement listProductPrice : productPrice) {
//			System.out.println(listProductPrice.getText());
//		}

		for (int i = 0; i < productName.size(); i++) {
			System.out.println(productName.get(i).getText() + "   " + productPrice.get(i).getText());
		}
		driver.findElementByXPath("(//div[@class='_3wU53n'])[1]").click();
		Set<String> windowFrom = driver.getWindowHandles();
		List<String> lstFrom = new ArrayList<>();
		lstFrom.addAll(windowFrom);
		driver.switchTo().window(lstFrom.get(1));
		String productTitle = driver.getTitle();
		String FirstProductName = driver.findElementByXPath("//span[@class='_35KyD6']").getText();
		String replacedText = FirstProductName.replaceAll("( .*)", "");
		if (productTitle.contains(replacedText)) {
			System.out.println("Both name are matching");
		} else {
			System.out.println("Both name are not matching");
		}
		WebElement eleRatings = driver.findElementByXPath("(//div[@class='col-12-12']/span)[1]");
		WebElement eleReviews = driver.findElementByXPath("(//div[@class='col-12-12']/span)[2]");
		System.out.println(eleRatings.getText());
		System.out.println(eleReviews.getText());
		driver.close();
	}

}
